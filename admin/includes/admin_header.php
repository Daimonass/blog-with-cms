<?php ob_start(); ?>
<?php session_start(); ?>
<?php include "../includes/db.php" ?>
<?php include "functions.php" ?>
<?php 

// Checking if transfer SESSION, and if user_role have 'admin', then only hes can see admin page. Right????
  if (!isset($_SESSION['user_role'])) {    
      header("Location: ../index.php");
  }elseif ($_SESSION['user_role'] !== 'admin') {
      header("Location: ../index.php");
  }

?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="">
    <meta name="author" content="">
    <title>SB Admin - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- Plugin CSS -->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/sb-admin.css" rel="stylesheet">

  </head>

  <body class="fixed-nav" id="page-top">