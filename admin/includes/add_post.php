<?php
if (isset($_POST['create_post'])) {

    $post_title = $_POST['title'];
    $post_author = $_POST['post_author'];
    $post_category_id = $_POST['post_category'];
    $post_status = $_POST['post_status'];

    $post_image = $_FILES['post_image']['name'];
    $post_image_temp = $_FILES['post_image']['tmp_name'];

    $post_tags = $_POST['post_tags'];
    $post_content = $_POST['post_content'];
    $post_date = date('y-m-d');

    move_uploaded_file($post_image_temp, "../images/$post_image");

    $query = "INSERT INTO posts(post_category_id, post_title, post_author, post_date, post_image, post_content, post_tags, post_status) ";

    $query .= "VALUE ({$post_category_id}, '{$post_title}','{$post_author}', now(), '{$post_image}', '{$post_content}', '{$post_tags}', '{$post_status}' ) ";

    $create_post_query = mysqli_query($connection, $query);

    // Quert Check
    confirmQuery($create_post_query);

    header("Location: posts.php");
}

?>

<form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="title">Post Title</label>
        <input type="text" class="form-control" id="title" name="title">
    </div>

    <div class="form-group">
        <label for="post_category">Post Category</label>
        <br>
        <select class="custom-select" name="post_category" id="post_category">
            <?php 
                $query = "SELECT * FROM categories";
                $select_categories = mysqli_query($connection, $query);
                
                confirmQuery($select_categories);

                while ($row = mysqli_fetch_assoc($select_categories)) {
                    $cat_id = $row['cat_id'];    
                    $cat_title = $row['cat_title'];    
                    echo "<option value='{$cat_id}'>{$cat_title}</option>";
                }
            ?>
        </select>
    </div>

    <div class="form-group">
        <label for="author">Author</label>
        <input type="text" class="form-control" id="author" name="post_author">
    </div>

    <div class="form-group">
        <label for="post_status">Post Status</label>
        <input type="text" class="form-control" id="post_status" name="post_status">
    </div>

    <div class="form-group">
        <label for="image">Add Image</label><br>
        <label for="image" class="custom-file">
            <span class="custom-file-control"></span>
            <input type="file" accept="image/*" id="image" class="custom-file-input" name="post_image">
        </label>
    </div>

    <!-- <div class="form-group">
        <label for="image">Add Image..</label>
        <input type="file" accept="image/*" class="form-control-file" id="image" name="post_image">
    </div> -->

    <div class="form-group">
        <label for="post_tags">Add post tags</label>
        <input type="text" class="form-control" id="post_tags" name="post_tags">
    </div>

    <div class="form-group">
        <label for="post_content">Add content</label>
        <textarea class="form-control" id="post_content" name="post_content" rows="10"></textarea>
    </div>

    <div class="form-group">
        <input class="btn btn-success" type="submit" name="create_post" value="Puslish Post">
    </div>
</form>
