<?php include "includes/admin_header.php" ?>

<!-- Navigation -->
<?php include "includes/admin_navigation.php" ?>

<div class="content-wrapper py-3">

    <div class="container-fluid">

        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.php">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Categories</li>
        </ol>
        <!-- END Breadcrumbs -->

        <div class="row">
            <div class="col-md-6">
                    <!-- ADD CATEGORIES -->
                <?php insertCategories(); ?>
                <form action="" method="post">
                    <div class="form-group">
                        <label for="category_title" class="col-form-label">Add Category</label>
                        <input class="form-control" type="text" name="cat_title" id="category_title" placeholder="Category name">
                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary" type="submit" value="SUBMIT" name="submit">
                    </div>
                </form>

                <!--   UPDATE AND INCLUDE QUERY -->
            <?php updateCategory(); ?>
            
            </div>
            <div class=" form-group col-md-6">
                <!-- Example Tables Card -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fa fa-table"></i> Categories
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" width="100%" id="dataTable" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Category Title</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!--  FIND ALL CATEGORIES -->
                                    <?php findAllCategories(); ?>

                                    <!-- DELETE QUERY -->
                                    <?php deleteCategory(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <!-- /.container-fluid -->

</div>
<!-- /.content-wrapper -->

<!-- Scroll to Top Button -->
<a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>

<!-- Logout Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                Select "Logout" below if you are ready to end your current session.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="login.html">Logout</a>
            </div>
        </div>
    </div>
</div>

<?php include "includes/admin_footer.php" ?>